package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HtmlTablesPage extends BasePage {
/**
 * @author yaelr
 * page: w3schools --> HTML Tables
 */
	@FindBy(xpath = "//h1[contains(text(),'HTML')]//span[contains(text(),'Tables')]")
	private WebElement pageHeader;

	@FindBy(id = "customers")
	private WebElement tblCustomers;

	public HtmlTablesPage(WebDriver driver) {
		super(driver);
	}

	public boolean isOnPage() {
		try {
			new WebDriverWait(driver, timeOut).until(ExpectedConditions.visibilityOf(pageHeader));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
/**
 * function will return cell value according to column parameter and row of search text
 * @param table - table to check
 * @param searchColumn - column id to search value
 * @param searchText - text to search
 * @param returnColumnText - column id from a value will return
 * @return value according to returnColumnText and row of search text
 */
	public String getTableCellTextByXpath(WebElement table, int searchColumn, String searchText, int returnColumnText) {
		new WebDriverWait(driver, timeOut).until(ExpectedConditions.visibilityOf(table));
		System.out.println("value from table is: " + table
				.findElement(By.xpath(".//td[text()='" + searchText + "']/parent::tr//td[" + returnColumnText + "]"))
				.getText());
		return table
				.findElement(By.xpath(".//td[text()='" + searchText + "']/parent::tr//td[" + returnColumnText + "]"))
				.getText();
	}
/**
 * function will compare cell value between cell of column parameter and row of search text and expected value 
 * @param table - table to check
 * @param searchColumn - column id to search value
 * @param searchText - text to search
 * @param returnColumnText - column id from a value will return
 * @param expectedText - text that should be in the cell
 * @return true if cell value as expected, otherwise false.
 */
	public boolean verifyTableCellText(WebElement table, int searchColumn, String searchText, int returnColumnText,
			String expectedText) {
		String columnValue = getTableCellTextByXpath(table, searchColumn, searchText, returnColumnText);

		return columnValue.equals(expectedText);
	}

}
