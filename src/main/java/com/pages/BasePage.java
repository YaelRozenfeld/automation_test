package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


/**
 * base page
 * @author yaelr
 *
 */
public abstract class BasePage {
	protected WebDriver driver;
	protected final int timeOut;
	
	public BasePage(WebDriver driver) {
		timeOut = 30;
		this.driver = driver;
		
		PageFactory.initElements(driver, this);

	}

}
