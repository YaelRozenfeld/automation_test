package com.test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.pages.HtmlTablesPage;
import com.utils.GmailUtils;



/**
 * List of things that should be modified in real project:
 * 1. paths - from property
 * 2. logs instead of println
 * 3. Dependfency injection
 * 4. Reporting mechanism
 * 5. Driver management
 * 6. When failed - should create screenshots and logs with extensions
 * 7. Versions in pom.xml as variables
 * @author yaelr
 *
 */
public class AutomationTest {
	public WebDriver driver;
	Properties prop = new Properties();

	@Before
	public void beforeTest() throws IOException {
		System.out.println("init driver");
		System.setProperty("webdriver.chrome.driver", "C:/Yael/test_project/src/main/resources/chromedriver.exe");
		driver = new ChromeDriver();
		// Perform Basic Operations
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get("https://www.w3schools.com/html/html_tables.asp");

		
		InputStream io = getClass().getClassLoader().getResourceAsStream("test.data.properties");
		if (io != null)
			prop.load(io);

	}

	@Test
	/**
	 * check if cell text from customer table is as expected
	 */
	public void verifyTableCellText() {
		HtmlTablesPage htmlTables = new HtmlTablesPage(driver);
		assertTrue("check exist on HTML Tables page",htmlTables.isOnPage());
		
		WebElement table = new WebDriverWait(driver, 30)
				.until(ExpectedConditions.visibilityOfElementLocated(By.id("customers")));
		
		assertTrue("check value in customers table, column 2 row of Island Trading is UK",
				htmlTables.verifyTableCellText(table, Integer.parseInt(prop.getProperty("search_column")),
						prop.getProperty("search_text"),
						Integer.parseInt(prop.getProperty("return_column_text")),
						prop.getProperty("expected_text")));
	}
	
	@Test
	/**
	 * get email from Gmail
	 */
	public void getEmailFromGmail() {
	      String host = "pop.gmail.com";// change accordingly
	      String mailStoreType = "pop3";
	      String username = "";// change accordingly
	      String password = "";// change accordingly

	      GmailUtils.check(host, mailStoreType, username, password);
	}

	@After
	public void afterTest() {
		driver.close();
		driver.quit();
	}
}
